<?php

return [
  'adminEmail'        => 'iseed12@rembler.ru',
  'debugLevel'        => getenv('DEBUG_LEVEL'),
  'tokenLength'       => 25,
  'minPasswordLength' => 6,
  'maxPasswordLength' => 45,
  'requestLimitCount' => 300,
  'requestLimitPeriod'=> 600,
];

