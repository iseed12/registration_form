<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
  'id'         => 'api',
  'basePath'   => dirname(__DIR__),
  'bootstrap'  => ['log'],
  'components' => [
    'request'    => [
      'cookieValidationKey' => 'ujf4udMnoyc4',
      'parsers'             => [
        'application/json' => 'yii\web\JsonParser',
      ]
    ],
    'response'   => [
      'format'        => yii\web\Response::FORMAT_JSON,
      'charset'       => 'UTF-8',
      'formatters' => [
        \yii\web\Response::FORMAT_JSON => [
          'class' => 'yii\web\JsonResponseFormatter',
          'prettyPrint' => YII_DEBUG,
          'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
        ],
      ],
    ],
    'cache'      => [
      'class' => 'yii\caching\FileCache',
    ],
    'user'       => [
      'identityClass'   => 'app\models\database\Users',
    ],
    'mailer'     => [
      'class' => 'yii\swiftmailer\Mailer',
    ],
    'log'        => [
      'traceLevel' => $params['debugLevel'],
      'targets'    => [
        [
          'class'   => 'yii\log\FileTarget',
          'levels'  => ['error', 'warning'],
          'logFile' => '@runtime/logs/error.log',
          'logVars' => ['_POST', '_GET']
        ],
        [
          'class'      => 'yii\log\FileTarget',
          'levels'     => ['info'],
          'logFile'    => '@runtime/logs/info.log',
          'categories' => ['application'],
          'logVars'    => []
        ]
      ],
    ],
    'db'         => $db,
    'urlManager' => [
      'enablePrettyUrl'     => true,
      'enableStrictParsing' => true,
      'showScriptName'      => false,
      'rules'               => [
        [
          'pluralize'     => false,
          'class'         => 'yii\rest\UrlRule',
          'controller'    => [
            'v1/accounts',
          ],
          'tokens'        => [
            '{id}' => '<id:\\w+>'
          ],
        ],
      ],
    ],
    'formatter'  => [
      'class'           => 'yii\i18n\Formatter',
      'dateFormat'      => 'php:Y-m-d',
      'datetimeFormat'  => 'php:Y-m-d H:i:s',
      'defaultTimeZone' => new \DateTimeZone('Asia/Novokuznetsk'),
      'timeZone'        => 'GMT+7',
      'timeFormat'      => 'php:H:i:s',
      'locale'          => 'ru-RU', //your language locale
    ],
  ],
  'container'  => [
    'definitions' => [
      \app\models\database\Accounts::class,
      \app\models\utils\UsersIdentify::class,
    ],
    'singletons'  => [
    ]
  ],
  'modules'    => [
    'v1' => [
      'class'               => 'app\modules\v1\Module',
      'controllerNamespace' => 'app\modules\v1\controllers',
    ],
  ],
  'params'     => $params,
];


return $config;
