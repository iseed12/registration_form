<?php
/**
 * Created by PhpStorm.
 * User: iseed
 * Date: 01.12.18
 * Time: 13:27
 */

namespace app\models\database;

use app\models\utils\Validator;
use yii\db\ActiveRecord;

/**
 * Class Accounts
 * Модель описывающая работу сущности Учетная запись.
 * @package app\models\database
 * @property integer $id
 * @property string nickname
 * @property string name
 * @property string last_name
 * @property string email
 * @property string password
 * @property integer rights_group_id
 * @property string token
 * @property string created_at
 * @property string updated_at
 */
class Accounts extends ActiveRecord
{
  const SCENARIO_CREATE = 'create';
  const SCENARIO_UPDATE = 'update';

  public static function tableName()
  {
    return 'accounts';
  }

  public function rules()
  {
    return [
      [['nickname', 'name', 'last_name', 'email', 'password'],
       'required',
       'message' => 'Обязательное поле не указано',
       'on' => [self::SCENARIO_CREATE]
      ],
      ['nickname',
       'unique',
       'targetAttribute' => 'nickname',
       'message' => 'Человек с таким никнеймом уже зарегистрирован',
       'on' => [self::SCENARIO_CREATE]
      ],
      ['email',
       'unique',
       'targetAttribute' => 'email',
       'message' => 'На этот ящик уже зарегистирован аккаунт',
       'on' => [self::SCENARIO_CREATE]
      ],
      ['password',
       'number',
       'min' => \Yii::$app->params['minPasswordLength'],
       'message' => 'Пожайлуста, выдумайте пароль больше 5 символов',
       'on' => [self::SCENARIO_CREATE]
      ],
      ['id',
       'integer',
       'message' => 'id учетной записи указано неверно',
      ],
      ['nickname',
       'match',
       'pattern' => Validator::PATTERN_NICKNAME,
       'message' => 'Никнейм учетной записи указан неверно',
      ],
      ['name',
       'match',
       'pattern' => Validator::PATTERN_FIO,
       'message' => 'Имя учетной записи указано неверно',
      ],
      ['last_name',
       'match',
       'pattern' => Validator::PATTERN_FIO,
       'message' => 'Фамилия учетной записи указано неверно',
      ],
      ['email',
       'email',
       'message' => 'Адрес электронной почты учетной записи указан неверно',
      ],
      ['password',
       'string',
       'message' => 'Пароль учетной записи указан неверно',
      ],
      ['rights_group_id',
       'integer',
       'message' => 'Группа права учетной записи указана неверно',
      ],
      ['token',
       'string',
       'message' => 'Ключ авторизации учетной записи указан неверно',
      ],
      ['created_at',
       'datetime',
       'message' => 'Дата создания учетной записи указана неверно',
      ],
      ['updated_at',
       'datetime',
       'message' => 'Дата обновления учетной записи указана неверно',
      ],
    ];
  }

  /**
   * Фильтрует вывод массивов для ответа.
   * Удаляет поле пароль и токен
   * @return array
   */
  public function fields()
  {
    $fields = parent::fields();
    unset($fields['password']);
    unset($fields['token']);
    return $fields;
  }

  /**
   * @return int
   */
  public function getId()
  {
    return $this->id;
  }

  /**
   * @param int $id
   * @return Accounts $this;
   */
  public function setId($id)
  {
    $this->id = $id;
    return $this;
  }

  /**
   * @return string
   */
  public function getNickname()
  {
    return $this->nickname;
  }

  /**
   * @param string $nickname
   * @return Accounts $this;
   */
  public function setNickname($nickname)
  {
    $this->nickname = $nickname;
    return $this;
  }

  /**
   * @return string
   */
  public function getName()
  {
    return $this->name;
  }

  /**
   * @param string $name
   * @return Accounts $this;
   */
  public function setName($name)
  {
    $this->name = $name;
    return $this;
  }

  /**
   * @return string
   */
  public function getLastName()
  {
    return $this->last_name;
  }

  /**
   * @param string $last_name
   * @return Accounts $this;
   */
  public function setLastName($last_name)
  {
    $this->last_name = $last_name;
    return $this;
  }

  /**
   * @return string
   */
  public function getEmail()
  {
    return $this->email;
  }

  /**
   * @param string $email
   * @return Accounts $this;
   */
  public function setEmail($email)
  {
    $this->email = $email;
    return $this;
  }

  /**
   * @return string
   */
  public function getPassword()
  {
    return $this->password;
  }

  /**
   * @param string $password
   * @return Accounts $this;
   */
  public function setPassword($password)
  {
    $this->password = $password;
    return $this;
  }

  /**
   * @return int
   */
  public function getRightsGroupId()
  {
    return $this->rights_group_id;
  }

  /**
   * @param int $rights_group_id
   * @return Accounts $this;
   */
  public function setRightsGroupId($rights_group_id)
  {
    $this->rights_group_id = $rights_group_id;
    return $this;
  }

  /**
   * @return string
   */
  public function getToken()
  {
    return $this->token;
  }

  /**
   * @param string $token
   * @return Accounts $this;
   */
  public function setToken($token)
  {
    $this->token = $token;
    return $this;
  }

  /**
   * @return string
   */
  public function getCreatedAt()
  {
    return $this->created_at;
  }

  /**
   * @param string $created_at
   * @return Accounts $this;
   */
  public function setCreatedAt($created_at)
  {
    $this->created_at = $created_at;
    return $this;
  }

  /**
   * @return string
   */
  public function getUpdatedAt()
  {
    return $this->updated_at;
  }

  /**
   * @param string $updated_at
   * @return Accounts $this;
   */
  public function setUpdatedAt($updated_at)
  {
    $this->updated_at = $updated_at;
    return $this;
  }

}