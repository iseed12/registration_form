<?php
/**
 * Created by PhpStorm.
 * User: iseed
 * Date: 01.12.18
 * Time: 14:17
 */

namespace app\models\utils;

use yii\base\BaseObject;
use yii\filters\RateLimitInterface;

/**
 * Реализация проверки количесва запросов для определеннгого IP пользвоателя
 * Class UsersIdentify
 * @package app\models\database
 * @property integer allowance
 * @property integer allowance_updated_at
 */
class UsersIdentify extends BaseObject implements RateLimitInterface
{
  public $allowance = 0;
  public $allowance_updated_at;

  /**
   * Проверяет наличие записи о пользователе по ip
   * @return UsersIdentify|null|static
   */
  public static function checkRateLimit()
  {
    $model = self::findIdentity();
    if(empty($model))
      $model = self::createNew();
    return $model;
  }

  /**
   * Находит пользователя из кеша по ip запроса
   * @return null|static
   */
  public static function findIdentity()
  {
    $item = \Yii::$app->getCache()->get(\Yii::$app->getRequest()->getUserIP());
    if(empty($item))
      return null;
    return $item;
  }

  /**
   * Создает новую запись в кеше о пользователе
   * @return UsersIdentify
   */
  public static function createNew()
  {
    $now = new \DateTime();
    /** @var \DateTimeZone $timezone */
    $timezone = \Yii::$app->getFormatter()->defaultTimeZone;
    $now->setTimezone($timezone);
    $model = new self();
    $model->setAllowanceUpdatedAt($now->getTimestamp());
    \Yii::$app->getCache()->set(\Yii::$app->getRequest()->getUserIP(), $model);
    return $model;
  }

  /**
   * Устанавливает расчетные значения лимитирования
   * @param \yii\web\Request $request
   * @param \yii\base\Action $action
   * @return array
   */
  public function getRateLimit($request, $action)
  {
    return [\Yii::$app->params['requestLimitCount'], \Yii::$app->params['requestLimitPeriod']];
  }

  /**
   * Устанавливает предидущие значения лимитирования
   * @param \yii\web\Request $request
   * @param \yii\base\Action $action
   * @return array
   */
  public function loadAllowance($request, $action)
  {
    return [$this->allowance, $this->allowance_updated_at];
  }

  /**
   * Сохраняет новые значения в кеше.
   * @param \yii\web\Request $request
   * @param \yii\base\Action $action
   * @param int $allowance
   * @param int $timestamp
   */
  public function saveAllowance($request, $action, $allowance, $timestamp)
  {
    $this->allowance = $allowance;
    $this->allowance_updated_at = $timestamp;
    \Yii::$app->getCache()->set(\Yii::$app->getRequest()->getUserIP(), $this);
  }

  /**
   * @return int
   */
  public function getAllowance()
  {
    return $this->allowance;
  }

  /**
   * @param int $allowance
   * @return UsersIdentify $this;
   */
  public function setAllowance($allowance)
  {
    $this->allowance = $allowance;
    return $this;
  }

  /**
   * @return int
   */
  public function getAllowanceUpdatedAt()
  {
    return $this->allowance_updated_at;
  }

  /**
   * @param int $allowance_updated_at
   * @return UsersIdentify $this;
   */
  public function setAllowanceUpdatedAt($allowance_updated_at)
  {
    $this->allowance_updated_at = $allowance_updated_at;
    return $this;
  }

}