<?

/** Версия 1.2 Исправил методы на статический вызов.
 * Class Validator
 */

namespace app\models\utils;

use \Exception as ValidException;

class Validator
{
  const PATTERN_DATE = '/^\d{4}-\d{2}-\d{2}$/';
  const PATTERN_DATE_BITRIX = '/^\d{2}.\d{2}.\d{4}$/';
  const PATTERN_DATETIME_BITRIX = '/^\d{2}.\d{2}.\d{4}(\s\d{2}:\d{2}:\d{2})*$/';
  const PATTERN_DATETIME = '/^\d{4}-\d{2}-\d{2}(\s\d{2}:\d{2}:\d{2})*$/';
  const PATTERN_INTEGER = '/^-*\d+$/';
  const PATTERN_DECIMAL = '/^\d+(\.\d{2})*$/';
  const PATTERN_CONTRACT_TITLE = '/^(o|un)*\d+$/';
  const PATTERN_STRING = '/^[А-яA-z0-9\s\-\/\_\,\.\;\:\?\=\!\#\(\)@\%\*\+\№ёЁ]+$/u';
  const PATTERN_FIO = '/^[А-я\-ёЁ]+$/u';
  const PATTERN_LATIN_STRING = '/^[A-z0-9\s\-\/\_\,\.\;\:\?\=\!\#\(\)@\%\*\+\№]+$/';
  const PATTERN_EMAIL = '/^[A-z0-9\_\.\-\!\,]+\@[A-z0-9_]+\.(com|ru|net)$/';
  const PATTERN_FLOAT = '/^\d+\.\d+$/';
  const PATTERN_TOKEN = '/^\d+\.\d{2}$/';
  const PATTERN_QUERY = '/^(\;\:|\#|UNION|\<|\?|\>)+$/';
  const PATTERN_IP = '/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/';
  const PATTERN_PHONE = '/^\+*(7|8)\d{10}$/';
  const PATTERN_MAC = '/^([A-F0-9]{2}\:*){6}$/';
  const PATTERN_CHAR_UPPER = '/^[A-ZА-Я]{1,}$/';
  const PATTERN_CHAR_DIGIT = '/^[0-9]{1,}$/';
  const PATTERN_CHAR_SPECIAL = '/^[\_\-\.\,\%]{1,}$/';
  const PATTERN_BITRIX_CODE = '/^[A-z]{1,}$/';
  const PATTERN_ADDRESS = '/^[0-9А-я\s\-]+\s[0-9]{1,4}[а-я\/0-9]*\s[0-9а-я\-\/]{1,4}$/u';
  const PATTERN_YEAR = '/^[0-9]{4}$/';
  const PATTERN_MONTH = '/^(0\d|1[012])$/';
  const PATTERN_DAY = '/^(0[1-9]|[12]\d|3[01])$/';
  const PATTERN_ON_OFF = '/^(0|1)$/';
  const PATTERN_IP_MASK = '/^([1-2]*[0-9]{1}|(3[0-2]))$/';
  const PATTERN_ID_LIST = '/^[0-9,]+$/';
  const PATTERN_YEAR_LIST = '/^([0-9]{4},?)+$/';
  const PATTERN_NICKNAME = '/^[A-z]{1}[A-z0-9]*$/';

  /** Проверяет является ли строка месяцем
   * @param $sDay - Строка с днём.
   * @param int $code - Код ошибки в случае ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isDay($sDay, $code = 0)
  {
    $message = empty($message) ? "День " . $sDay . " указан не правильно. " . self::PATTERN_DAY : $message;
    if (!preg_match(self::PATTERN_DAY, $sDay))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка месяцем
   * @param $sMonth - Строка с месяцем.
   * @param int $code - Код ошибки в случае ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isMonth($sMonth, $code = 0)
  {
    $message = empty($message) ? "Месяц " . $sMonth . " указан не правильно. " . self::PATTERN_MONTH : $message;
    if (!preg_match(self::PATTERN_MONTH, $sMonth))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка годом
   * @param $sYear - Строка с годом.
   * @param int $code - Код ошибки в случае ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isYear($sYear, $code = 0)
  {
    $message = empty($message) ? "Год " . $sYear . " указан не правильно. " . self::PATTERN_YEAR : $message;
    if (!preg_match(self::PATTERN_YEAR, $sYear))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверка на не пустые значения
   * @param $params - Массив с параметрами.
   * @param $validParams - Массив ключей которые должны быть.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isNotEmpty($params, $validParams, $message = '', $code = 0)
  {
    foreach ($validParams as $param) {
      if (!isset($params[$param])) {
        $message = empty($message) ? "Обьязательный параметр не указан." : $message;
        throw new ValidException($message, $code);
      }
    }
    return true;
  }

  /** Проверяет является ли строка датой PHP
   * @param $sDate - Строковая дата.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isDate($sDate, $message = '', $code = 0)
  {
    $message = empty($message) ? "Дата " . $sDate . " неверна." . self::PATTERN_DATE : $message;
    if (!preg_match(self::PATTERN_DATE, $sDate))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка датой Битрикс
   * @param $sDate - Строковая дата.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isDateBitrix($sDate, $message = '', $code = 0)
  {
    $message = empty($message) ? "Дата " . $sDate . " неверна." . self::PATTERN_DATE_BITRIX : $message;
    if (!preg_match(self::PATTERN_DATE_BITRIX, $sDate))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка датой и временем PHP
   * @param $sDateTime - Строковая дата и время.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isDateTime($sDateTime, $message = '', $code = 0)
  {
    $message = empty($message) ? "Дата " . $sDateTime . " неверна." . self::PATTERN_DATETIME : $message;
    if (!preg_match(self::PATTERN_DATETIME, $sDateTime))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка датой и временем Битрикс
   * @param $sDateTime - Строковая дата и время.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isDateTimeBitrix($sDateTime, $message = '', $code = 0)
  {
    $message = empty($message) ? "Дата " . $sDateTime . " неверна." . self::PATTERN_DATETIME_BITRIX : $message;
    if (!preg_match(self::PATTERN_DATETIME_BITRIX, $sDateTime))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли целочисленным.
   * @param $iNum - Строковое число.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isInteger($iNum, $message = '', $code = 0)
  {
    $message = empty($message) ? "Число " . $iNum . " неверно." . self::PATTERN_INTEGER : $message;
    if (!preg_match(self::PATTERN_INTEGER, $iNum))
      throw new ValidException($message, $code);
    return true;
  }

  /**
   * Проверяет электронную почту
   * @param $email
   * @param string $message
   * @param int $code
   * @return bool
   * @throws ValidException
   */
  public static function isEmail($email, $message = '', $code = 0)
  {
    $message = empty($message) ? "Адрес почты " . $email . " указан неверно." . self::PATTERN_INTEGER : $message;
    if (!preg_match(self::PATTERN_EMAIL, $email))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли числом с плавающей точкой.
   * @param $iNum - Строковое число.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isFloat($iNum, $message = '', $code = 0)
  {
    $message = empty($message) ? "Число " . $iNum . " неверно." . self::PATTERN_FLOAT : $message;
    if (!preg_match(self::PATTERN_FLOAT, $iNum))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строкой.
   * @param $sString - Строка.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isString($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Строка " . $sString . " неверна. Ожидалось: " . self::PATTERN_STRING : $message;
    if (!preg_match(self::PATTERN_STRING, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /**
   * Проверяет латинску строку
   * @param $sString
   * @param string $message
   * @param int $code
   * @return bool
   * @throws ValidException
   */
  public static function isLatinString($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Строка " . $sString . " неверна. Ожидалось: " . self::PATTERN_LATIN_STRING : $message;
    if (!preg_match(self::PATTERN_LATIN_STRING, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /**
   * Проверяет соотвествие фамилии
   * @param $sString
   * @param string $message
   * @param int $code
   * @return bool
   * @throws ValidException
   */
  public static function isFio($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Строка " . $sString . " неверна. Ожидалось: " . self::PATTERN_FIO : $message;
    if (!preg_match(self::PATTERN_FIO, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка ip адресом.
   * @param $sString - Строка.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isIp($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Ip " . $sString . " неверен. Ожидалось: " . self::PATTERN_IP : $message;
    if (!preg_match(self::PATTERN_IP, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка строкой договора биллинга.
   * @param $sString - Строка.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isContractTitle($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Номер договора " . $sString . " неверен. Ожидалось: " . self::PATTERN_CONTRACT_TITLE : $message;
    if (!preg_match(self::PATTERN_CONTRACT_TITLE, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка sql запросом. Типа разрешенные символя и тп.
   * @param $sString - Строка.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isSQLQuery($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Запрос " . $sString . " неверен. Ожидалось: " . self::PATTERN_QUERY : $message;
    if (preg_match(self::PATTERN_QUERY, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка токеном api portal.
   * @param $sString - Строка.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isToken($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Токен " . $sString . " неверен. Ожидалось: " . self::PATTERN_TOKEN : $message;
    if (!preg_match(self::PATTERN_TOKEN, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка телефоном.
   * @param $sString - Строка.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isPhone($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Телефон " . $sString . " неверен. Ожидалось: " . self::PATTERN_PHONE : $message;
    if (!preg_match(self::PATTERN_PHONE, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка mac адресом.
   * @param $sString - Строка.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isMac($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Мак " . $sString . " неверен. Ожидалось: " . self::PATTERN_MAC : $message;
    if (!preg_match(self::PATTERN_MAC, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяет является ли строка строковый адресом. Вида {Улица Дом Квартира}
   * @param $sString - Строка.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isAddress($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Адрес " . $sString . " неверна. Ожидалось: " . self::PATTERN_ADDRESS : $message;
    if (!preg_match(self::PATTERN_ADDRESS, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверка верности маски IP
   * @param $sString
   * @param string $message
   * @param int $code
   * @return bool
   * @throws ValidException
   */
  public static function isIpMask($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Маска Ip адреса " . $sString . " неверна. Ожидалось: " . self::PATTERN_IP_MASK : $message;
    if (!preg_match(self::PATTERN_IP_MASK, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверяем перечень id через запятую
   * @param $sString
   * @param string $message
   * @param int $code
   * @return bool
   * @throws ValidException
   */
  public static function isIdList($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Перечень ID " . $sString . " неверен. Ожидалось: " . self::PATTERN_ID_LIST : $message;
    if (!preg_match(self::PATTERN_ID_LIST, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /**
   * Проверяет на соответсвие публичное имя
   * @param $sString
   * @param string $message
   * @param int $code
   * @return bool
   * @throws ValidException
   */
  public static function isNickname($sString, $message = '', $code = 0)
  {
    $message = empty($message) ? "Публичное имя " . $sString . " неверено. Ожидалось: " . self::PATTERN_NICKNAME : $message;
    if (!preg_match(self::PATTERN_NICKNAME, $sString))
      throw new ValidException($message, $code);
    return true;
  }

  /** Проверка на длинну строки
   * @param $string - Строка.
   * @param $length - Длинна меньше котрой будет вызван exception.
   * @param string $message - Сообщение в случае ошибки.
   * @param int $code - Код ошибки в случа ошибки.
   * @return bool - Успешный возврат.
   * @throws ValidException
   */
  public static function isLength($string, $length, $message = '', $code = 0)
  {
    $message = empty($message) ? "Длинна параметра меньше " . $length : $message;
    if (mb_strlen($string) < $length)
      throw new ValidException($message, $code);
    return true;
  }

  /**
   * Проверяет наличие ключа в массиве
   * @param $key
   * @param $array
   * @param string $message
   * @param int $code
   * @return bool
   * @throws ValidException
   */
  public static function inArray($key, $array, $message = '', $code = 0)
  {
    $message = empty($message) ? "Параметр указан неверно. Ожидалось: " . implode(",", $array) : $message;
    if (!in_array($key, $array))
      throw new ValidException($message, $code);
    return true;
  }

  /**
   * Проверяет соответвие дат
   * @param $date1
   * @param $date2
   * @param string $message
   * @param int $code
   * @return bool
   * @throws ValidException
   */
  public static function dateCompare($date1, $date2, $message = '', $code = 0)
  {
    $date1 = new \DateTime($date1);
    $date2 = new \DateTime($date2);

    $message = empty($message) ? 'Даты не совпадают' : $message;
    if ($date1->getTimestamp() != $date2->getTimestamp())
      throw new ValidException($message, $code);

    return true;
  }
}