<?php

namespace app\modules\v1\controllers;

use app\models\database\Accounts;
use app\models\utils\UsersIdentify;
use yii\filters\RateLimiter;
use yii\rest\ActiveController;
use yii\web\Response;
use yii\filters\Cors;
use yii\data\ActiveDataFilter;
use yii\filters\ContentNegotiator;
use yii\base\DynamicModel;

class AccountsController extends ActiveController
{
  public $modelClass = Accounts::class;
  public $createScenario = Accounts::SCENARIO_CREATE;
  public $updateScenario = Accounts::SCENARIO_UPDATE;

  public function actions()
  {
    $actions = parent::actions();
    unset($actions['update'], $actions['delete']);
    $actions['index'] = [
      'class'       => 'yii\rest\IndexAction',
      'modelClass'  => $this->modelClass,
      'checkAccess' => [$this, 'checkAccess'],
      'dataFilter' => [
        'class' => ActiveDataFilter::class,
        'searchModel' => function () {
          return (new DynamicModel(['email' => null, 'nickname' => null]))
            ->addRule('email', 'email')
            ->addRule('nickname', 'string');
        },
      ],
    ];
    return $actions;
  }

  public function behaviors()
  {
    $behaviors = array_merge(parent::behaviors(), [
      'contentNegotiator' => [
        'class'   => ContentNegotiator::class,
        'formats' => [
          'application/json' => Response::FORMAT_JSON,
        ],
      ],
      'corsFilter'        => [
        'class' => Cors::class,
      ],
      //      'authenticator'     => [
      //        'class'    => QueryParamAuth::class,
      //        'optional' => ['options', 'login']
      //      ],
      'rateLimiter'       => [
        'class'        => RateLimiter::class,
        'errorMessage' => 'Превышено количество запросов. Попробуйте повторить позднее',
        'enableRateLimitHeaders' => false,
        'user' => UsersIdentify::checkRateLimit()
      ]
    ]);
    return $behaviors;
  }

}
