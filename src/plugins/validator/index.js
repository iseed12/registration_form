const validator = {
  install: function (Vue) {
    const integer = /^-*[0-9]+$/;
    const integerList = /^-*[0-9,]+$/;
    const float = /^-*[0-9]+\.[0-9]+$/;
    const summa = /^-*[0-9]+(\.[0-9]{1,2})*$/;
    const string = /^[A-zА-я0-9,!;:_#%+\?\(\)\.\-\"\'\*\/\\\s]+$/u;
    const nickname = /^[A-z]{1}[A-z0-9]*$/;
    const fio = /^[А-я\s\-]+$/u;
    const ip = /^([0-9]{1,3}\.){3}\.[0-9]{1,3}$/;
    const email = /^[A-z0-9_\-\.]+@[A-z0-9_\-\.]+\.(com|ru|net)$/;
    const letters = /^[A-z0-9_]+$/;
    const date = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/;
    const datetime = /^[0-9]{4}\-[0-9]{2}\-[0-9]{2}\s[0-9]{2}:[0-9]{2}$/;
    const month = /^[0-9]{4}\-[0-9]{2}$/;
    const phone = /^(7|8)[0-9]{10}$/;
    const classname = /^[A-z\\]+$/;
    const imageFile = /^[A-zА-я0-9\s_\-]+\.(jpg|jpeg|gif|png)$/u;
    const webLink = /^(https*:\/\/)*[A-zА-я0-9_#\-\/\.]+$/;
    const routerLink = /^\/#\/[A-z0-9\/\-]+$/;
    const contract = /^(o|un)*[0-9]+$/;
    const password = /^[A-z0-9]+$/;
    const newPassword = /^[A-z0-9]{5,45}$/;
    const flat = /^[а-я0-9\/]+$/;
    const code = /^[0-9]{4}$/;

    Vue.prototype.isEmpty = (value, message = 'Значение не указано.') => (value !== null && value !== undefined && value.toString().trim() !== '') || message;
    Vue.prototype.isInteger = (value, message = 'Число указано неверно.', ) => (integer.test(value) || message);
    Vue.prototype.isIntegerList = (value, message = 'Список чисел указан неверно.') => (integerList.test(value) || message);
    Vue.prototype.isFloat = (value, message = 'Число указано неверно.') => (float.test(value) || message);
    Vue.prototype.isString = (value, message = 'Строка имеет неверные символы.') => (string.test(value) || message);
    Vue.prototype.isNickname = (value, message = 'Никнейм указан неверно') => (nickname.test(value) || message);
    Vue.prototype.isFio = (value, message = 'Имя указано неверно.') => (fio.test(value) || message);
    Vue.prototype.isIp = (value, message = 'Ip адрес указан неверно.') => (ip.test(value) || message);
    Vue.prototype.isEmail = (value, message = 'Почта указана неверно.') => (email.test(value) || message);
    Vue.prototype.isLetters = (value, message = 'Строка указана неверно.') => (letters.test(value) || message);
    Vue.prototype.isDate = (value, message = 'Дата указана неверно.') => (date.test(value) || message);
    Vue.prototype.isDateTime = (value, message = 'Дата указана неверно.') => (datetime.test(value) || message);
    Vue.prototype.isMonth = (value, message = 'Дата указана неверно.') => (month.test(value) || message);
    Vue.prototype.isPhone = (value, message = 'Номер телефона указан неверно.') => (phone.test(value) || message);
    Vue.prototype.isClassname = (value, message = 'Имя класса указано неверно.') => (classname.test(value) || message);
    Vue.prototype.isImageFile = (value, message = 'Файл не является изображением.') => (imageFile.test(value) || message);
    Vue.prototype.isWebLink = (value, message = 'Ссылка указана неверно.') => (webLink.test(value) || message);
    Vue.prototype.isRouterLink = (value, message = 'Маршрут указан неверно.') =>  (routerLink.test(value) || message);
    Vue.prototype.isContract = (value, message = 'Номер договора указан неверно.') => (contract.test(value) || message);
    Vue.prototype.isPassword = (value, message = 'Пароль указан неверно.') => (password.test(value) || message);
    Vue.prototype.isNewPassword = (value, message = 'Пароль указан неверно.') => (newPassword.test(value) || message);
    Vue.prototype.isFlat = (value, message = 'Квартира указана неверно.') => (flat.test(value) || message);
    Vue.prototype.isSumma = (value, message = 'Сумма указана неверно.') => (summa.test(value) || message);
    Vue.prototype.isSmsCode = (value, message = 'Код подтверждения указан неверно.') => (code.test(value) || message);
    Vue.prototype.inRange = (value, min, max, message = null) => {
      if(message === null)
        message = 'Значение должно быть в диапазоне от ' + min + ' до ' + max + '.';
      if(value < min || value > max)
        return message;
      return true;
    }
    Vue.prototype.isLength = (value, min, max, message = null) => {
      if(message === null)
        message = 'Длинна должна быть в диапазоне от ' + min + ' до ' + max + '.';
      if(value.length < min || value.length > max)
        return message;
      return true;
    }
  }
};

export default validator