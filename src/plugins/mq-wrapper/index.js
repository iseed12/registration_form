import Vue from 'vue'

Vue.mixin({
  methods : {
    mqpack(params, single) {
      let style = [];
      if(params.length === 5){
        if(this.$mq.xs)
          style.push(params[0]);
        if(this.$mq.sm)
          style.push(params[1]);
        if(this.$mq.md)
          style.push(params[2]);
        if(this.$mq.lg)
          style.push(params[3]);
        if(this.$mq.xl)
          style.push(params[4]);
      } else if(params.length === 4) {
        if(this.$mq.xs || this.$mq.sm)
          style.push(params[0]);
        if(this.$mq.md)
          style.push(params[1]);
        if(this.$mq.lg)
          style.push(params[2]);
        if(this.$mq.xl)
          style.push(params[3]);
      } else if(params.length === 3) {
        if(this.$mq.xs || this.$mq.sm || this.$mq.md)
          style.push(params[0]);
        if(this.$mq.lg)
          style.push(params[1]);
        if(this.$mq.xl)
          style.push(params[2]);
      } else if(params.length === 2) {
        if(this.$mq.xs || this.$mq.sm || this.$mq.md || this.$mq.lg)
          style.push(params[0]);
        if(this.$mq.xl)
          style.push(params[1]);
      } else if(params.length === 1) {
        style.push(params[0]);
      }
      return single ? style[0] : style;
    },
  }
});