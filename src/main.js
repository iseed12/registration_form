import Vue from 'vue'
import Vuex from 'vuex/dist/vuex.min'
import VueResource from 'vue-resource/dist/vue-resource.min';
import Vuetify from 'vuetify/dist/vuetify.min'
import Router from './router'
import App from './App.vue'
import Store from './store'
import MQ from 'vue-match-media/dist'
import Validator from './plugins/validator'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
require('./plugins/mq-wrapper');

Vue.use(VueResource);
Vue.use(MQ);
Vue.use(Vuetify);
Vue.use(Vuex);
Vue.use(Validator);
Vue.use(Vuetify, {
  iconfont: 'mdi'
})

if (process.env.NODE_ENV === 'production') {
  Vue.config.devtools = false;
  Vue.config.debug = false;
  Vue.config.silent = true;
}

new Vue({
  el: '#app',
  template: '<App/>',
  router: Router,
  store: Store,
  components: {
    'App': App,
  },
  mq: {
    xs: '(max-width: 599px)',
    sm: '(min-width: 600px) and (max-width: 960px)',
    md: '(min-width: 961px) and (max-width:  1264px)',
    lg: '(min-width: 1265px) and (max-width: 1904px)',
    xl: '(min-width: 1905px)',
  },
});
