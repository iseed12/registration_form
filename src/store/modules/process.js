const state = [];

const mutations = {
    createProcess(state, id){
        let item = { id : id, message : null, status : null};
        state.forEach((entry, i) => {
            if(entry.id === item.id){
                state.splice(i,1);
            }
        });
        state.push(item);
    },
    destroyProcess(state, id){
        state.forEach((entry, i) => {
            if(entry.id === id){
                state.splice(i,1);
            }
        });
    },
    setProcessSuccess(state, id){
        state.forEach(entry => {
            if(entry.id === id){
                entry.message = null;
                entry.status = true;
            }
        });
    },
    setProcessFailed(state, params){
        state.forEach(entry => {
            if(entry.id === params.id){
                entry.status = false;
                entry.message = params.message;
                entry.code = params.code;
            }
        });
    }
};

const getters = {
    getProcessError: (state) => (id) => {
      let item = state.filter(status => status.id === id);
      if(item.length === 0)
        return null
      return item[0].message;
    },
    getProcessStatus: (state) => (id) => {
        let item = state.filter(status => status.id === id);
        if(item.length === 0)
            return null;
        return item[0].status;
    },
};

const actions = {
    createProcess({ commit }, id){
        commit('createProcess', id);
    },
    destroyProcess({ commit }, id){
        commit('destroyProcess', id );
    },
    setProcessSuccess({ commit }, id){
        commit('setSuccess', id);
    },
    setProcessFailed({ commit }, params){
        commit('setFailed', params);
    }
};

export default {
    state,
    getters,
    mutations,
    actions
}