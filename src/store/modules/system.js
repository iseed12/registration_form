//const DOMAIN_URL = 'http://api.registration.local';
const DOMAIN_URL = 'http://api.' + window.location.hostname;
const API_URL = DOMAIN_URL + "/";

const state = {};
const mutations = {
  changeSystemParams(state, items) {
    state = Object.assign(state, items)
  },
};

const getters = {
  getDomainUrl: () => DOMAIN_URL,
  getApiUrl: () => API_URL
};
const actions = {};

export default {
  state,
  mutations,
  getters,
  actions,
}