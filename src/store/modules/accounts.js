import Vue from 'vue'

const state = {
  existNickname: false,
  existsEmail: false,
};
const mutations = {
  changeAccountsParams(state, items) {
    state = Object.assign(state, items)
  },
};

const getters = {
  getAccountsExistsNickname: state => state.existNickname,
  getAccountsExistsEmail: state => state.existsEmail,
};
const actions = {
  findByNickname({commit, getters}) {
    commit('createProcess', 'findByNickname');
    let url = getters.getApiUrl + 'v1/accounts?filter[nickname]=' + getters.getAccountNickname;
    Vue.http.get(url).then(response => {
      let account = response.data;
      commit('changeAccountsParams', {existNickname: account.length > 0});
      commit('setProcessSuccess', 'findByNickname');
    }, response => {
      commit('setProcessFailed', {id: 'findByNickname', message: response.body.message});
    });
  },
  findByEmail({commit, getters}) {
    commit('createProcess', 'findByEmail');
    let url = getters.getApiUrl + 'v1/accounts?filter[email]=' + getters.getAccountEmail;
    Vue.http.get(url).then(response => {
      let account = response.data;
      commit('changeAccountsParams', {existsEmail: account.length > 0});
      commit('setProcessSuccess', 'findByEmail');
    }, response => {
      commit('setProcessFailed', {id: 'findByEmail', message: response.body.message});
    });
  }
};

export default {
  state,
  mutations,
  getters,
  actions,
}