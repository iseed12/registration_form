import Vue from 'vue'

const state = {
  nickname: '',
  name: '',
  last_name: '',
  email: '',
  password: ''
};
const mutations = {
  changeAccountParams(state, items) {
    state = Object.assign(state, items)
  },
  setAccountBlank: (state) => {
    state.nickname = '';
    state.name = '',
    state.last_name = '',
    state.email = '',
    state.password = '',
    delete state.id;
  },
};

const getters = {
  getAccountNickname: state => state.nickname,
  getAccountName: state => state.name,
  getAccountLastname: state => state.last_name,
  getAccountEmail: state => state.email,
  getAccountPassword: state => state.password,
};

const actions = {
  setAccountNickname: ({commit}, value) => commit('changeAccountParams', {nickname : value }),
  setAccountName: ({commit}, value) => commit('changeAccountParams', {name : value}),
  setAccountLastname: ({commit}, value) => commit('changeAccountParams', {last_name : value }),
  setAccountEmail: ({commit}, value) => commit('changeAccountParams', {email : value }),
  setAccountPassword: ({commit}, value) => commit('changeAccountParams', {password : value }),
  createNewAccount({commit, getters, state}) {
    commit('createProcess', 'createAccount');
    Vue.http.post(getters.getApiUrl + 'v1/accounts', state).then(response => {
      let account = response.data;
      commit('changeAccountParams', account);
      commit('setProcessSuccess', 'createAccount');
    }, response => {
      commit('setProcessFailed', {id: 'createAccount', message: response.body.message});
    });
  },
  setAccountBlank({commit}){
    commit('setAccountBlank');
  }
};

export default {
  state,
  mutations,
  getters,
  actions,
}