import Vue from 'vue'
import Vuex from 'vuex'

import Account from './modules/account.js'
import Accounts from './modules/accounts.js'
import System from './modules/system.js'
import Process from './modules/process.js'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        Account,
        Accounts,
        System,
        Process,
    }
})