import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

//Секция сайт
const StartPage = () => import(/* webpackChunkName: "group-site" */ './../pages/site/StartPage.vue');
const RegistrationPage = () => import(/* webpackChunkName: "group-site" */ './../pages/site/RegistrationPage.vue');

export default new Router({
  routes: [
    {
      path: '/',
      component: StartPage,
      meta: {title: 'Сайт - главная'},
    },
    {
      path: '/accounts/registration',
      component: RegistrationPage,
      meta: {title: 'Сайт регистрация'},
    }
  ]
});